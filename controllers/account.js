
// ## dependencies
var express = require('express');

var ledger = require('../lib/ledger');
var view = require('../lib/view');

// ## config
const router = express.Router();

// ## exp
var ns = { router: router };
module.exports = ns;

// ## functions
ns.groupAccounts = async function(req, res, next){
	
	var acc = await ledger.getAccounts();
	
	var maxLevel = 0;
	acc.forEach(function(x){
		if( x.level > maxLevel ){
			maxLevel = x.level;
		}
	});
	
	var reorder = acc.filter(x => x.level === 0);
	for(var i=1; i<=maxLevel; i++){
		var current = acc.filter(x => x.level === i);
		var offset = {};
		current.forEach(function(x){
			var ind = reorder.findIndex( b => b._id === x.parentId );
			if( !offset[x.parentId] ){
				offset[x.parentId] = 0;
			}
			
			reorder.splice(ind+1+offset[x.parentId], 0, x);
			offset[x.parentId] += 1;
		});
	}
	
	console.log('groupAccounts - end = %j', reorder);
	res.locals.accounts = reorder;
	return next();
};

ns.renderAccount = function(req, res){
	var page = view.render('accounts', res.locals);
	return res.send(page);
};

// ## routes
router.get('/', ns.groupAccounts, ns.renderAccount);


