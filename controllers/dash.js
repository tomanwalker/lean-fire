
// ## dependencies
var express = require('express');

var ledger = require('../lib/ledger');
var view = require('../lib/view');
var logLib = require('../lib/log');

var contBudget = require('./budget');

// ## config
const router = express.Router();
var log = logLib.getLogger('con/dash');

// ## export
var ns = {router: router};
module.exports = ns;

// ## fund
ns.getBudget = async function(req, res, next){
	var bud = await ledger.getBudget();
	res.locals.budget = bud;
	return next();
};
ns.getCashFlow = async function(req, res, next){
	
	var periods = contBudget.getPastPeriods();
	var acc = await ledger.getAccounts({base: true});
	
	for(var p of periods){
		
		p.obj = {};
		
		for(var a of acc){
			p.obj[a.name] = await ledger.getBalance(a._id, p.end);
		}
	}
	
	res.locals.flow = periods;
	res.locals.flow_str = JSON.stringify(periods, null, 2);
	res.locals.flow_one = JSON.stringify(periods);
	//log.debug('getCashFlow - acc = %j', acc);
	log.debug('getCashFlow - result = %j', periods);
	return next();
};

ns.render = function(req, res){
	var page = view.render('dash', res.locals);
	return res.send(page);
};

// ## routes
router.get('/', ns.getBudget, ns.getCashFlow, ns.render);


