
// ## dependencies
var express = require('express');

var ledger = require('../lib/ledger');
var view = require('../lib/view');
var contApi = require('./api');

// ## config
const router = express.Router();

// ## exp
var ns = { router: router };
module.exports = ns;

// ## functions
ns.renderJournal = function(req, res){
	var page = view.render('journal', res.locals);
	return res.send(page);
};

// ## routes
router.get('/', ns.renderJournal); //contApi.getEntriesReverse, 


