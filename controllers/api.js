
// ## dependencies
var express = require('express');

var ledger = require('../lib/ledger');
var logLib = require('../lib/log');

// ## config
var log = logLib.getLogger('api');
var router = express.Router();

// ## exp
var ns = { router: router };
module.exports = ns;

// ## functions
ns.getAccounts = async function(req, res, next){
	var accounts = await ledger.getAccounts();
	log.debug('getAccounts - done - len = %s', accounts.length);
	res.locals.accounts = accounts;
	return next();
};
ns.getEntriesReverse = async function(req, res, next){
	
	var entries = await ledger.getEntriesReverse();
	
	entries = entries.map(function(x){
		x.date = x.ts.slice(0,10);
		return x;
	});
	
	res.locals.entries = entries;
	return next();
};

// ## routes
router.post('/transfer', ns.getAccounts, async function(req, res){
	
	log.debug('post.transfer - start - body = %j', req.body);
	//{ date: "2022-08-19", "acc-src": "Cash", "acc-dst": "Cafe", category: "flexible", amount: 100 }
	
	var accounts = res.locals.accounts;
	var acc_src = accounts.find( x => x.name === req.body['acc_src'] );
	var acc_dst = accounts.find( x => x.name === req.body['acc_dst'] );
	
	var result = await ledger.move({
		amount: req.body.amount,
		credit: acc_src.code,
		debit: acc_dst.code
	});
	
	return res.json(result);
});

router.get('/entries', ns.getEntriesReverse, function(req, res){
	return res.json(res.locals.entries);
});

router.get('/accounts', async function(req, res){
	var result = await ledger.getAccounts();
	return res.json(result);
});

router.post('/entries', ns.getAccounts, async function(req, res){
	log.debug('post.entries - start - body = %j', req.body);
	
	var accounts = res.locals.accounts;
	
	var entries = req.body.map(function(e){
		var accObj = accounts.find( x => x.name === e.account );
		e.account = accObj.code;
		return e;
	});
	
	var result = await ledger.post(entries);
	return res.json(result);
});

router.get('/budget', async function(req, res){
	var result = await ledger.getBudget();
	return res.json(result);
});


