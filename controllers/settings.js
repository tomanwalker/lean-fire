
// ## dependencies
var express = require('express');

var view = require('../lib/view');
var logLib = require('../lib/log');

// ## config
const router = express.Router();
var log = logLib.getLogger('con/budget');

// ## exp
var ns = { router: router };
module.exports = ns;

// ## functions

// ## routes
router.get('/', function(req, res){
	var page = view.render('settings', res.locals);
	return res.send(page);
});


