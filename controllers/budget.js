
// ## dependencies
var express = require('express');

var ledger = require('../lib/ledger');
var view = require('../lib/view');
var logLib = require('../lib/log');
var utils = require('../lib/utils');

// ## config
const router = express.Router();
var log = logLib.getLogger('con/budget');

// ## exp
var ns = { router: router };
module.exports = ns;

// ## functions
ns.getPastPeriods = function(){
	var periodStart = 1;
	var monthLimit = 6;
	log.debug('getPastPeriods - start - start = %s | limit = %s', periodStart, monthLimit);
	
	var list = [];
	var check = new Date();
	
	for(var i=0; i<monthLimit; i++){
		
		var date = new Date();
		date.setMonth(date.getMonth() - i);
		date.setDate(periodStart);
		date.setHours(0, 0, 0, 0);
		
		var end = new Date(date.getTime());
		end.setMonth(end.getMonth()+1);
		end.setDate(periodStart-1);
		end.setHours(23, 59, 59, 999);
		
		//log.debug('getPastPeriods - loop - i = %s | start = %s | end = %s', 
		//	i, utils.shortDate(date), utils.shortDate(end));
		
		var date2 = new Date(date.getTime());
		date2.setHours(12, 0, 0, 0);
		list.push({start: date, end: end, month: date2.toISOString().slice(0,7) });
	}
	
	return list;
};

ns.filterHasDate = function(list){
	return list.filter(function(x){
		for(var cat in x.bud){
			if( x.bud[cat].actual > 0 ){
				return true;
			}
		}
		return false;
	});
};

ns.lastMonthStats = function(list){
	
	var statMap = {
		"income": "actual",
		"necessity": "actual_p",
		"savings": "actual_p"
	};
	var stats = [];
	
	for(var cat in statMap){
		var att = statMap[cat];
		var sum = list.map(x => x.bud[cat][att]).reduce((a,b)=>a+b);
		var avg = sum / list.length;
		var num_unit = (att === 'actual_p') ? '%' : '';
		
		stats.push({ cat: cat, avg: (avg + num_unit) });
	}
	
	return stats;
};

ns.groupBudgetMonth = async function(req, res, next){
	
	var dateList = ns.getPastPeriods();
	log.debug('groupBudget - start - periods = %j', dateList );
	
	for(var dat of dateList){
		dat.bud = await ledger.getBudget(dat.start, dat.end);
	}
	
	//var bud = await ledger.getBudget(dateList[0].start, dateList[0].end);
	//log.debug('loop - %j', dateList);
	
	res.locals.bud_list = dateList;
	res.locals.stats = ns.lastMonthStats(ns.filterHasDate(dateList));
	return next();
};

// ## routes
router.get('/', ns.groupBudgetMonth, function(req, res){
	var page = view.render('budget', res.locals);
	return res.send(page);
});


