
// ## dependencies
var express = require('express');

var ledger = require('./lib/ledger');
var view = require('./lib/view');

var journalCont = require('./controllers/journal');
var contApi = require('./controllers/api');
var contAccount = require('./controllers/account');
var contBudget = require('./controllers/budget');
var contSet = require('./controllers/settings');
var contDash = require('./controllers/dash');

var config = require('./config');

// ## config
var server = express();
server.use(express.json());

// ## functions
var init = async function(){
	await ledger.init();
};

// ## routes
server.get('/', function(req, res){
	return res.redirect('/dash');
});

server.use('/api', contApi.router);

server.use('/dash', contDash.router);
server.use('/journal', journalCont.router);
server.get('/entry', async function(req, res){
	res.locals.inputRows = [0, 1];
	var page = view.render('entry', res.locals);
	return res.send(page);
});

server.use('/accounts', contAccount.router);
server.use('/budget', contBudget.router);
server.use('/settings', contSet.router);

server.get('/balance', contAccount.groupAccounts, async function(req, res){
	
	var acc = res.locals.accounts;
	//var baseAcc = acc.filter(x => x.level === 0);
	
	for(var a of acc){
		var bal = await ledger.getBalance(a._id);
		a.balance = bal.balance;
		a.cnt = bal.cnt;
	}
	
	var nonEmpty = acc.filter(function(x){
		return (
			x.cnt > 0
			|| x.level === 0
			|| x.virtual
		);
	});
	
	res.locals.accounts = nonEmpty;
	res.locals.income = nonEmpty.filter(x => x.code.includes('inc'));
	res.locals.expense = nonEmpty.filter(x => x.code.includes('exp'));
	res.locals.asset = nonEmpty.filter(x => x.code.includes('ast'));
	res.locals.liability = nonEmpty.filter(x => x.code.includes('lia'));
	res.locals.equity = nonEmpty.filter(x => x.code.includes('eq'));
	
	var page = view.render('balance', res.locals);
	return res.send(page);
});

server.get('/forecast', async function(req, res){
	res.locals.test = 'abc';
	var page = view.render('forecast', res.locals);
	return res.send(page);
});

server.get('/state', async function(req, res){
	// Affordability, etc.
	res.locals.test = 'abc';
	var page = view.render('forecast', res.locals);
	return res.send(page);
});

var filesPath = __dirname + '/html';
console.log('filesPath = %s', filesPath);
server.use('/', express.static(filesPath));

// ## flow
init();

console.log('main - server start - port = %s', config.PORT);
server.listen(config.PORT, function(){
	console.log('main - server started...');
});


