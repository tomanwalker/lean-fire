
var depoy = (process.env.DEPLOY === 'true');

if( !depoy ){
	var secret = require('./secret.json');
	for(var p in secret){
		if( typeof(process.env[p]) === 'undefined' ){
			process.env[p] = secret[p];
		}
	}
}

module.exports = {
	PORT: Number(process.env.PORT || 9000),
	DEPLOY: depoy,
	DB_ENGINE: 'mongo', // 'memory' | 'file_csv'
	MONGO_HOST: process.env.MONGO_HOST || 'localhost',
	MONGO_PORT: process.env.MONGO_PORT || 27017,
	MONGO_USER: process.env.MONGO_USER,
	MONGO_PASS: process.env.MONGO_PASS,
	MONGO_DB: process.env.MONGO_DB || 'budget-dev'
};
