
// ## dependencies
var fs = require('fs');
var ms = require('mustache');

// ## config
var VIEW_EXT = 'html';
ms.escape = function(txt){return txt;}; // avoid html escape

// ## export
var ns = {};
module.exports = ns;

// ## functions
ns.render = function(page, bag, template){
	
	if( typeof(template) === 'undefined' ){
		var template = 'template';
	}
	var mainContent = '{{body}}';
	if( template ){
		mainContent = fs.readFileSync('views/' + template + '.' + VIEW_EXT, 'utf8');
	}
	
	var pageContent = (
		'<div class="row"><div class="col-lg-12 text-center"><br /><br />'
		+ '<h3>Something went wrong. Try again later</h3>'
		+ '</div></div>'
	);
	try{
		pageContent = fs.readFileSync('views/' + page + '.' + VIEW_EXT, 'utf8');
	}
	catch(err){
		throw err;
	}
	
	/*
	for(var p in bag){
		var piece = bag[p];
		if( typeof(piece) === 'object' ){
			piece = JSON.stringify(piece);
		}
		
		console.log('lib/view.render - replace - k = %s | v = %s', p, piece);
		pageContent = pageContent.replace('{{' + p + '}}', piece);
	}
	*/
	var result = ms.render(pageContent, bag);
	var merged = mainContent.replace('{{body}}', result);
	
	return merged;
};


