
// ## dependencies
var fs = require('fs');
var fastcsv = require('fast-csv');

// ## export
var ns = {};
module.exports = ns;

// ## funcs
ns.readFile = async function(filename, opts){
	
	if( typeof(opts) === 'undefined' ){
		opts = {};
	}
	if( typeof(opts.del) === 'undefined' ){
		opts.del = ";";
	}
	
	console.log('lib/readFile - start - file = %s', filename);
	
	var data =null;
	try{
		
		var content = fs.readFileSync(filename);
		data = [];
		parser_obj = fastcsv.parseString(content, {
			delimiter: opts.del,
			headers: true
		});
		
		var prom = new Promise(function(resolve, reject){
			parser_obj.on('error', error => console.error(error))
				.on('data', row => data.push(row))
				.on('end', rowCount => resolve(data));
		});
		
		await prom;
		
		//console.log('lib/readFile - loaded - sample = %j', data[0]);
	}
	catch(err){
		if( err.code === "ENOENT" ){
			data = [];
			console.log('lib/readFile - catch - file does not exist = %s', filename );
		}
		else {
			console.log('lib/readFile - catch - err = %j | msg = %s', err, (err.msg || err) );
		}
	}
	
	return data;
};

ns.writeFile = async function(filename, data, dry){
	console.log('lib/writeFile - before - name = %s | cnt = %s', filename, data.length);
	
	if( dry ){
		return true;
	}
	
	var dir = filename.split('/').slice(0, -1).join('/');
	if( !fs.existsSync(dir) ){
		console.log('lib/writeFile - no directory yet = %s', dir);
		fs.mkdirSync(dir);
	}
	
	var content = await fastcsv.writeToString(data, { headers: true, delimiter: ';' });
	fs.writeFileSync(filename, content);
	
	return data.length;
};


