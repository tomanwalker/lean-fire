
// ## dependencies

var utils = require('../utils');

// ### export
var ns = {};
module.exports = ns;

// ## config
var memoryStore = {};

// ## fund
ns.generateId = function(){
	var date = new Date();
	var ts = date.toISOString();
	var formatted = ts.slice(0,19).replace(/\-/g, '').replace('T', '-').replace(/\:/g, '');
	var id = [formatted, utils.random(100, 999)].join('-');
	return id;
};
ns.table = function(tableName){
	if( !memoryStore[tableName] ){
		memoryStore[tableName] = [];
	}
	
	return memoryStore[tableName];
};

ns.put = function(tableName){
	return function(obj){
		
		if( !obj.id ){
			obj.id = ns.generateId();
		}
		
		memoryStore[tableName].push(obj);
		return obj;
	};
};

ns.search = function(query){
	return function(x){
		for(var p in query){
			if( query[p]["$includes"] ){
				if( !x[p].includes( query[p]["$includes"] ) ){
					return false;
				}
			}
			else if( query[p] !== x[p] ){
				return false;
			}
		}
		
		return true;
	};
};

ns.find = function(tableName){
	return function(query){
		return memoryStore[tableName].filter( ns.search(query) );
	};
};
ns.findOne = function(tableName){
	return function(query){
		return memoryStore[tableName].find( ns.search(query) );
	};
};


