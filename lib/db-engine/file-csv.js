
// ## dependencies
var csv = require('../csv');
var utils = require('../utils');

// ### export
var ns = {};
module.exports = ns;

// ## config
var dir = './data/';

var schema = {
	'account': {
		'level': 'number',
		'base': 'boolean'
	},
	'journal': {
		'credit': 'number',
		'debit': 'number',
		'ts': 'date'
	}
};

// ## fund
ns.table = function(tableName){
	// will be created on first Write
	return tableName;
};

ns.readTable = async function(tableName){
	var tableFile = dir + tableName + '.csv';
	var data = await csv.readFile(tableFile);
	if( schema[tableName] ){
		var tableSchema = schema[tableName];
		data = data.map(function(x){
			
			for(var a in tableSchema){
				if( tableSchema[a] === 'number' && !isNaN(x[a]) ){
					x[a] = Number(x[a]);
				}
				if( tableSchema[a] === 'boolean' ){
					x[a] = ( x[a] === 'true' );
				}
			}
			
			return x;
		});
	}
	return data;
};

ns.searchCompare = function(data, q, sign){
	var test = q;
	if( typeof(test) !== typeof(data) ){
		test = test.toISOString();
	}
	
	if( sign === 'g' ){
		return data > test;
	}
	else {
		return data < test;
	}
};
ns.search = function(query){
	return function(x){
		
		var qKeys = Object.keys(query);
		if( qKeys.length === 0 ){
			return true;
		}
		
		var good = [];
		
		//console.log('DEBUG - search - q = %j | x = %j', query, x);
		for(var p in query){
			
			if( utils.isPrimitive(query[p]) ){
				good.push( query[p] === x[p] );
			}
			else{
				if( query[p]["$includes"] ){
					good.push( x[p].includes( query[p]["$includes"] ) );
				}
				if( query[p]["$gte"] ){
					
					var compStrG = ns.searchCompare(x[p], query[p]["$gte"], 'g');
					//console.log('DEBUG - x = %s | q = %s | r = %s', x[p], query[p]["$gte"], compStrG);
					good.push( compStrG );
				}
				if( query[p]["$lte"] ){
					
					var compStrL = ns.searchCompare(x[p], query[p]["$lte"], 'l');
					good.push( compStrL );
				}
			}
		}
		
		var allGood = good.every( g => g);
		//console.log('DEBUG - search = q = %j | g = %s | good = %s', query, good, allGood);
		return allGood;
	};
};

ns.find = function(tableName){
	return async function(query, params){
		
		if( !params ){
			params = {};
		}
		if( !params.limit ){
			params.limit = 25;
		}
		
		var data = await ns.readTable(tableName);
		var subset = data.filter( ns.search(query) );
		
		if( params.sort ){
			//console.log('DEBUG - before = %j', subset);
			var sortKeys = Object.keys(params.sort);
			if( sortKeys.length > 0 ){
				subset.sort(function(a,b){
					
					for(var i=0; i< sortKeys.length; i++){
						var sortPropKey = sortKeys[i];
						var sortPropVal = params.sort[sortPropKey];
						var gt = (a[sortPropKey] > b[sortPropKey]);
						var lt = (a[sortPropKey] < b[sortPropKey]);
						var pos = (sortPropVal === 1 && gt) || (sortPropVal === -1 && lt );
						var neg = (sortPropVal === -1 && gt) || (sortPropVal === 1 && lt );
						
						if( pos ){
							return 1;
						}
						if( neg ){
							return -1;
						}
					}
					
					return 0;
				});
			}
			//console.log('DEBUG - after = %j', subset);
		}
		
		subset = subset.slice(0, params.limit);
		
		return {
			count: data.length,
			rows: subset
		};
		
	};
};

ns.findOne = function(tableName){
	return async function(query){
		
		var data = await ns.readTable(tableName);
		return data.find( ns.search(query) );
	};
};

ns.put = function(tableName){
	return async function(obj){
		
		if( !obj.id ){
			obj.id = utils.getRandomString(14);
		}
		console.log('DEBUG - put - obj = %j', obj);
		var tableFile = dir + tableName + '.csv';
		var data = await csv.readFile(tableFile);
		data.push(obj);
		await csv.writeFile(tableFile, data);
		
		return obj;
		
	};
};


