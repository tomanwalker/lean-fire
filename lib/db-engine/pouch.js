
// ## dependencies
var PouchDB = require('pouchdb');

// ### export
var ns = {};
module.exports = ns;

// ## config
var db_dict = {};

// ## fund
ns.createTable = function(tableName){
	if( !db_dict[tableName] ){
		db_dict[tableName] = new PouchDB('data/' + tableName);
	}
};

ns.put = function(tableName){
	
	ns.createTable(tableName);
	return function(obj){
		
		return db_dict[tableName].put(obj);
		
	};
};


