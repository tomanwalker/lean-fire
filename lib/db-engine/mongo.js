
// ## dependencies
var mongo = require('mongodb');

var utils = require('../utils');
var config = require('../../config');

// ### export
var ns = {};
module.exports = ns;

// ## config
var MongoClient = mongo.MongoClient;
var creds = `${config.MONGO_USER}:${config.MONGO_PASS}`;
var url = `mongodb://${creds}@${config.MONGO_HOST}:${config.MONGO_PORT}/`;
var client = null;
var db = null;

// ## fund
ns.table = async function(tableName){
	
	if( client === null ){
		console.log('mongo.table - client.null - db init...');
		client = new MongoClient(url);
		await client.connect();
		db = client.db(config.MONGO_DB);
	}
	
	cols = await db.listCollections().toArray();
	console.log('mongo.table - list = %j', cols);
	if( !cols.find( x => x.name === tableName ) ){
		await db.createCollection(tableName);
	}
	
};

ns.find = function(tableName){
	return async function(query, params){
		var table = db.collection(tableName);
		//console.log('mongo.find - start - t = %s | query = %j', tableName, query);
		
		for(var p in query){
			if( query[p] && query[p]['$includes'] ){
				var temp = query[p]['$includes'];
				query[p] = { "$regex": temp };
			}
			if( p === 'ts' ){
				if( query[p]['$gte'] ){
					query[p]['$gte'] = query[p]['$gte'].toISOString();
				}
				if( query[p]['$lte'] ){
					query[p]['$lte'] = query[p]['$lte'].toISOString();
				}
			}
		}
		console.log('mongo.find - parsed - t = %s | query = %j | params = %j', tableName, query, params);
		
		var searchObj = table.find(query);
		
		if( params ){
			if( params.sort ){
				searchObj = searchObj.sort(params.sort);
			}
			if( params.limit ){
				searchObj = searchObj.limit(params.limit);
			}
		}
		
		var result = await searchObj.toArray();
		console.log('mongo.find - end - result = %j', result.length);
		return { rows: result };
	};
};

ns.findOne = function(tableName){
	return async function(query){
		console.log('mongo.findOne - start - t = %s | q = %j', tableName, query);
		var table = db.collection(tableName);
		var result = await table.findOne(query);
		console.log('mongo.findOne - end - result = %j', result);
		return result;
	};
};

ns.put = function(tableName){
	return async function(doc){
		console.log('mongo.put - start - t = %s | d = %j', tableName, doc);
		var table = db.collection(tableName);
		
		if( !doc._id ){
			doc._id = utils.getRandomString(14);
		}
		var result = await table.insertOne(doc);
		console.log('mongo.put - end - result = %j', result);
		return doc;
	};
};


