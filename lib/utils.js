
//## dep
var crypto = require("crypto");

//## config

//## export
var ns = {};
module.exports = ns;

// ## funs
ns.random = function (min, max) {
	return Math.floor(Math.random() * (max - min + 1) + min)
};

ns.getRandomString = function(len){
	if( isNaN(len) && len < 1 ){
		len = 10;
	}
	return crypto.randomBytes(len).toString('hex');
};

ns.isPrimitive = function(test){
	return test !== Object(test);
}

ns.displayRootOnly = function(obj){
	var display = {};
	for(var p in obj){
		display[p] = ns.isPrimitive(obj[p]) ? obj[p] : typeof(obj[p]);
	}
	return display;
};

ns.shortDate = function(date){
	return date.toISOString().slice(0,10);
};

ns.getMonthStart = function(date, periodStart){
	
	if( typeof(periodStart) === 'undefined' ){
		periodStart = 1;
	}
	
	var start = new Date(date.getTime());
	start.setDate(periodStart);
	start.setHours(0, 0, 0, 0);
	return start;
};


