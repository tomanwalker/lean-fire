
// ## dependencies
var memory = require('./db-engine/memory');
var file_csv = require('./db-engine/file-csv');
var pouch = require('./db-engine/pouch');
var mongo = require('./db-engine/mongo');

var config = require('../config');

// ### export
var ns = {};
module.exports = ns;

// ## config
var tables = [
	{ name: 'book' },
	{ name: 'account' },
	{ name: 'journal' }
];
ns.dict = {};

// ## fund
ns.init = async function(){
	console.log('dba.init - start - list = %j', tables);
	for( var x of tables ){
		console.log('dba.init - loop - table = %j', x);
		var t = x.name;
		var eng = x.engine || config.DB_ENGINE;
		
		ns.dict[t] = {};
		ns[t] = ns.dict[t];
		var targetEngine = memory;
		
		if( eng === 'pouch' ){
			targetEngine = pouch;
		}
		if( eng === 'file_csv' ){
			targetEngine = file_csv;
		}
		if( eng === 'mongo' ){
			targetEngine = mongo;
		}
		
		await targetEngine.table(t);
		
		ns.dict[t].find = targetEngine.find(t);
		ns.dict[t].findOne = targetEngine.findOne(t);
		
		ns.dict[t].put = targetEngine.put(t);
		ns.dict[t].save = targetEngine.put(t);
		ns.dict[t].insert = targetEngine.put(t);
		
	}
};

// ## flow
//ns.init();


