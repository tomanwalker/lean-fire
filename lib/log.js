
// ## dependencies

// ## config

// ## export
var ns = {};
module.exports = ns;

// ## functions
ns.getLogger = function(name){
	
	var print = function(...msg){
		msg[0] = name + ' >> ' + msg[0];
		return console.log(...msg);
	};
	
	return {
		debug: print,
		info: print,
		error: print
	};
};


