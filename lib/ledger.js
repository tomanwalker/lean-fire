
// ## dependencies

var logLib = require('./log');
var dba = require('./dba');
var utils = require('./utils');

// ## config
var log = logLib.getLogger('lib/ledger');

// ## export
var ns = {};
module.exports = ns;

// ## functions
ns.defineAccounts = async function(){
	
	var list = [
		// Base
		{ name: 'Income', code: 'inc'},
		{ name: 'Expense', code: 'exp'},
		{ name: 'Asset', code: 'ast'},
		{ name: 'Liability', code: 'lia'},
		{ name: 'Equity', code: 'eqt'},
		// Income
		{ name: 'Salary', code: 'inc:sal', parentCode: 'inc' },
		// Expenses
		{ name: 'Housing', code: 'exp:house', parentCode: 'exp', category: 'necessity' },
		{ name: 'Food', code: 'exp:food', parentCode: 'exp', category: 'necessity' },
		{ name: 'Transport', code: 'exp:transport', parentCode: 'exp', category: 'necessity' },
		
		{ name: 'Cafe', code: 'exp:cafe', parentCode: 'exp', category: 'flexible' },
		{ name: 'Other', code: 'exp:other', parentCode: 'exp', category: 'flexible' },
		{ name: 'Clothes', code: 'exp:clothes', parentCode: 'exp', category: 'flexible' },
		// Assets
		{ name: 'Cash', code: 'ast:cash', parentCode: 'ast' },
		{ name: 'Checking', code: 'ast:ch', parentCode: 'ast' },
		{ name: 'Savings', code: 'ast:sav', parentCode: 'ast', category: 'savings' },
		{ name: 'Fund', code: 'ast:fund', parentCode: 'ast', category: 'savings', interest: 4 },
		{ name: 'Travel', code: 'ast:travel', parentCode: 'ast', category: 'flexible' },
		// Liabilities
		{ name: 'Credit card', code: 'lia:card', parentCode: 'lia' },
		// Equity
		{ name: 'Adjust', code: 'eqt:adj', parentCode: 'eqt' },
		{ name: 'Capital', code: 'eqt:cap', parentCode: 'eqt', virtual: true },
		
		// Advanced
		{ name: 'A/receivable', code: 'ast:ar', parentCode: 'ast' },
		{ name: 'A/payable', code: 'lia:ap', parentCode: 'lia' },
		{ name: 'Defered revenue', code: 'lia:dr', parentCode: 'lia' },
		{ name: 'Asset depreciation', code: 'exp:ad', parentCode: 'exp' }
		
	];
	
	var maxLevel = 0;
	list = list.map(function(x){
		
		if( typeof(x.category) === 'undefined' ){
			x.category = null;
		}
		x.level = ( x.code.match(/\:/g) || [] ).length;
		x.base = ( x.level === 0 );
		
		x.parentCode = (x.parentCode || null);
		x.category = (x.category || null);
		x.parentId = null;
		if( x.level > maxLevel ){
			maxLevel = x.level;
		}
		
		return x;
	});
	
	var baseList = list.filter(x => x.level === 0);
	for(var s of baseList){
		var res = await dba.account.insert( s );
		s._id = res._id;
	}
	
	var firstLevel = list.filter(function(x){
		
		var matches = ( x.code.match(/\:/g) || [] );
		return matches.length === 1 
		
	});
	for(var s of firstLevel){
		var par = list.find( x => x.code === s.parentCode );
		s.parentId = par._id;
		s = await dba.account.insert( s );
	}
	
	return list;
};

ns.getAccounts = async function(query){
	if( typeof(query) === 'undefined' ){
		query = {};
	}
	var result = await dba.account.find(query);
	return result.rows;
};

ns.init = async function(opts){
	log.info('init - start - opts = %j', opts);
	
	if( typeof(opts) === 'undefined' ){
		opts = {};
	}
	if( typeof(opts.name) === 'undefined' ){
		opts.name = 'home';
	}
	
	log.info('init - parsed - opts = %j', opts);
	
	await dba.init();
	var book = await dba.book.findOne({ name: opts.name });
	log.debug('init - find - book = %j', book);
	
	if( !book ){
		book = await dba.book.insert({ name: opts.name });
	}
	
	// check accounts
	var acc = await ns.getAccounts();
	log.debug('init - acc - cnt = %s', acc.length);
	
	if( acc.length === 0 ){
		acc = await ns.defineAccounts();
	}
	
	book.accounts = acc;
	log.info('init - defined - book = %j', utils.displayRootOnly(book));
	return book;
	
};

ns.getEntries = async function(query, params){
	if( typeof(query) === 'undefined' ){
		query = {};
	}
	
	var result = await dba.journal.find(query, params);
	return result.rows;
};

ns.getEntriesReverse = function(query){
	
	if( typeof(query) === 'undefined' ){
		query = {};
	}
	
	var params = { sort: {ts: -1 }, limit: 20 };
	return ns.getEntries(query, params);
};

ns.calculateSum = function(entries){
	var sum = 0;
	entries.forEach(function(x){
		sum += (x.debit || 0) - (x.credit || 0);
	});
	if( sum < 0 ){
		sum = sum * (-1);
	}
	return sum;
};

ns.getBalance = async function(accountId, endDate){
	
	if( typeof(endDate) === 'undefined' ){
		endDate = new Date();
		endDate.setMonth(endDate.getMonth()+6);
	}
	
	log.debug('getBalance - start - id = %s', accountId);
	var account = await dba.account.findOne({ _id: accountId });
	log.debug('getBalance - found - code = %s / %s', accountId, account.code);
	var entries = await ns.getEntries({ 
		account: {"$includes": account.code },
		ts:{ "$lte": endDate }
	});
	log.debug('getBalance - found - entries = %s / %s', accountId, entries.length);
	
	var sum = ns.calculateSum(entries);
	
	//{ name: 'Capital', code: 'eqt:cap', parentCode: 'eqt', virtual: true },
	if( ['eqt', 'eqt:cap'].includes(account.code) ){
		var assets = await ns.getEntries({ 
			account: {"$includes": 'ast' },
			ts:{ "$lte": endDate }
		});
		var lia = await ns.getEntries({ 
			account: {"$includes": 'lia' },
			ts:{ "$lte": endDate }
		});
		
		var sumA = ns.calculateSum(assets);
		var sumB = ns.calculateSum(lia);
		sum = sumA - sumB;
	}
	
	return {cnt: entries.length, balance: sum};
};

ns.roundPercent = function(num){
	return Math.round(num * 10000) / 100;
};

ns.getBudget = async function(start, end){
	
	if( typeof(end) === 'undefined' ){
		end = new Date();
	}
	if( typeof(start) === 'undefined' ){
		start = utils.getMonthStart(end);
	}
	
	var budget = {
		income: { expected_p: '', actual: 0 },
		"necessity" : {expected_p: 50, actual: 0, actual_p: 0},
		"flexible": {expected_p: 30, actual: 0, actual_p: 0},
		"savings": {expected_p: 20, actual: 0, actual_p: 0}
	};
	
	var incomeList = await ns.getEntries({ account: {"$includes": 'inc' }, ts:{"$gte": start, "$lte": end} });
	console.log( 'DEBUG - start = %s | income = %j', start, incomeList );
	var incomeTotal = ns.calculateSum(incomeList);
	budget.income.actual = incomeTotal;
	
	var remaining = incomeTotal;
	var expKeys = Object.keys(budget).filter(k => k !== 'income');
	for(var k of expKeys){
		
		var arr = await ns.getEntries({ category: k, ts:{"$gte": start, "$lte": end} });
		var sum = ns.calculateSum(arr);
		budget[k].actual = sum;
		remaining = remaining - sum;
		
		if( incomeTotal !== 0 ){
			budget[k].actual_p = ns.roundPercent(sum / incomeTotal);
		}
	}
	
	budget.untracked = {
		actual: remaining,
		actual_p: 0
	};
	if( incomeTotal !== 0 ){
		budget.untracked.actual_p = ns.roundPercent(remaining / incomeTotal);
	}
	
	return budget;
};

ns.post = async function(arr){
	
	var date = new Date();
	var group = utils.getRandomString(10);
	
	var accountDict = {};
	
	for(var x of arr){
		
		['debit', 'credit'].forEach(function(a){
			if( typeof(x[a]) === 'undefined' ){
				x[a] = null;
			}
		});
		if( !accountDict[x.account] ){
			accountDict[x.account] = await dba.account.findOne({code: x.account});
		}
		if( !x.ts ){
			x.ts = date.toISOString();
		}
		if( !x.category ){
			x.category = accountDict[x.account].category;
		}
		
		x.group = group;
		
		await dba.journal.insert(x);
	}
	
	return true;
};

ns.move = function(obj){
	return ns.post([
		{ account: obj.credit, credit: obj.amount, comment: obj.comment, category: obj.category },
		{ account: obj.debit, debit: obj.amount, comment: obj.comment, category: obj.category },
	]);
};

// ## flow


