
//## dependencies
var assert = require('assert');

//## config
var unit = require('../lib/ledger');

//## flow
describe('Transfers', function () {
	
	var budgetBefore = null;
	
	before(async function(){
		
		this.timeout(1000);
		var book = await unit.init();
		
		budgetBefore = await unit.getBudget();
		await unit.move({ amount: 400, credit: "inc:sal", debit: "ast:ch" });
		
	});
	
	it('>> Transfer to savings - then transfer back', async function(){
		
		await unit.move({ amount: 100, credit: "ast:ch", debit: "ast:sav" });
		await unit.move({ amount: 100, credit: "ast:sav", debit: "ast:ch" });
		
		var budgetAfter = await unit.getBudget();
		assert.equal(budgetAfter.savings.actual, budgetBefore.savings.actual);
	});
	it('>> Transfer to savings - then buy asset', async function(){
		
		await unit.move({ amount: 100, credit: "ast:ch", debit: "ast:sav" });
		await unit.move({ amount: 100, credit: "ast:sav", debit: "ast:fund" });
		
		var budgetAfter = await unit.getBudget();
		assert.equal(budgetAfter.savings.actual, budgetBefore.savings.actual + 100);
	});
	it('>> Transfer to separate - then pay flexible', async function(){
		
		await unit.move({ amount: 100, credit: "ast:ch", debit: "ast:travel" });
		await unit.move({ amount: 100, credit: "ast:travel", debit: "exp:clothes" });
		
		var budgetAfter = await unit.getBudget();
		assert.equal(budgetAfter.flexible.actual, budgetBefore.flexible.actual + 100);
	});
	
	it('check Badget after', async function(){
		
		var budgetAfter = await unit.getBudget();
		
		console.table(budgetBefore);
		console.log('-------');
		console.table(budgetAfter);
		
	});
	
});


