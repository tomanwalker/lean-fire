
//## dependencies
var assert = require('assert');

//## config
var unit = require('../lib/ledger');

//## func
var balanceMap = function(x){
	return {
		name: x.name,
		code: x.code,
		balance: x.balance,
		cnt: x.cnt
	};
};

var delay = function(ms){
	return new Promise(function(resolve, reject){
		setTimeout(function(){
			return resolve(true);
		}, ms);
	});
};

//## flow
describe('Basic month', function () {
	it('init', async function(){
		
		this.timeout(1000);
		//await delay(500);
		var book = await unit.init();
		
		assert.equal(book.name, 'home');
		assert.equal(book.accounts.length, 21);
		
	});
	
	it('ls accounts', async function(){
		var acc = await unit.getAccounts();
		console.log('acc cnt = %s', acc.length);
		assert.equal( acc.length > 0, true);
	});
	
	it('check balance - before', async function(){
		var acc = await unit.getAccounts();
		var baseAcc = acc.filter(x => x.base);
		
		for(var a of baseAcc){
			var bal = await unit.getBalance(a._id);
			a.balance = bal.balance;
			a.cnt = bal.cnt;
		}
		
		console.table( baseAcc.map(balanceMap) );
		assert.equal( baseAcc[2].balance, 0 );
		
	});
	
	it('check budget - before', async function(){
		
		var bud = await unit.getBudget();
		console.table(bud);
		assert.equal( bud.savings.actual_p, 0 );
		assert.equal( bud.necessity.actual_p, 0 );
		assert.equal( bud.flexible.actual_p, 0 );
	});
	
	it('add transactions', async function(){
		
		await unit.post([
			{ account: "inc:sal", credit: 1000 },
			{ account: "ast:ch", debit: 1000, comment: 'one' },
		]);
		
		await unit.move({
			amount: 250,
			credit: "ast:ch",
			debit: "ast:sav"
		});
		
		await unit.move({ amount: 400, credit: "ast:ch", debit: "exp:house" });
		await unit.move({ amount: 100, credit: "ast:ch", debit: "exp:food" });
		
		await unit.move({ amount: 100, credit: "ast:ch", debit: "exp:cafe" });
		await unit.move({ amount: 50, credit: "ast:ch", debit: "exp:other" });
		
	});
	it('check journal', async function(){
		
		var result = await unit.getEntries();
		var forPrint = result.map(function(x){
			
			var obj = {
				account: x.account,
				debit: x.debit,
				credit: x.credit,
				group: x.group,
				category: (x.category || ''),
				ts: x.ts.slice(0,10),
				comment: (x.comment || "")
			};
			
			return obj;
		});
		console.table(forPrint);
		assert.equal( forPrint.length, 12 );
		
	});
	it('check balance - after', async function(){
		
		var baseAcc = await unit.getAccounts({ base: true });
		for(var a of baseAcc){
			var bal = await unit.getBalance(a._id);
			a.balance = bal.balance;
			a.cnt = bal.cnt;
		}
		console.table( baseAcc.map(balanceMap) );
		
		assert.equal( baseAcc[0].balance, 1000 );
		assert.equal( baseAcc[1].balance, 650 );
		assert.equal( baseAcc[2].balance, 350 );
		assert.equal( baseAcc[3].balance, 0 );
		assert.equal( baseAcc[4].balance, 350 );
		
	});
	it('check budget - after', async function(){
		
		var bud = await unit.getBudget();
		console.table(bud);
		
		assert.equal( bud.savings.actual_p, 25 );
		assert.equal( bud.necessity.actual_p, 50 );
		assert.equal( bud.flexible.actual_p, 15 );
	});
	
});


