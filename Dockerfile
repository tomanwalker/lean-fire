FROM node:lts-slim

ENV PORT 80
WORKDIR /app
EXPOSE ${PORT}

COPY . .
RUN npm install

CMD ["npm", "start"]



