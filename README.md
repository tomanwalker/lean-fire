# lean-fire

simple & flexible financial tracker   
- percentage based budgeting   
- Double entry accounting (DEA)   
- WEB application   
- Multiple database support   

## Quickstart

- Initialize   
- Add / remove accounts   
- Adjust budget   
- Create transactions   
- Check reports   

## Backlog
### TODO
```

- Fix: Budget page fails if empty journal

- Budget suggestion
- Ammortize bucket budget based on multiple incomes per month?
- Ammortize bills (Virtual / actual balance)?
- Deffered revenue

- Deploy & Test

- Journal page - "prevent enter" make more flexible behaviour
- Journal page - add/delete Rows animation
- Multitenant setup (DB-change, Auth, Validation, testing, sonarqube, API expose)
- more Dashboard widgets

- Deploy

- Account page - Create/Update accounts
- Import / Export journals CSV / Excel?
- Forecast page
- Asset depreciation automatically

- Deploy

- Forecast (Forecast Capital - avg income / expenses)
- Home affordability, Debt repay, Goals, FU/FI, Car affordability, etc. reports / suggestions
- Daily allowance?
- Session last page remember & redirect

- Add Financial ratios (Debt-to-Income, etc.) to Balance / Dashboard
- add Vue.js?


```
### DONE

[2023-05]
- Entry page - Green Notify as OK

[2023-04]
- Entry page - Delete extra rows after submit
- Entry page - Need to accept decimal
- Deploy & Test

[2022-10]
- Deploy
- Forecast (Empty page)
- Dashboard Cash Flow Graph (Money left on balance month-to-month)

[2022-09]
- Suggest Savings contribution amount based on Budget difference
- Accounts Balance (Ledger) page
- Entry (advance input) Page - multiple splits for a transaction

[2022-08]
- Testing for transfer cases
- separate DB (MongoDB)
- Dockerfile
- User / Org / Book Settings Page (fiscal period??)
- Budget page - 6 month history and Averages
- Account page - table list
- Basic Dash page
- Basic Journal page (Quick entries)
- Basic flow logic testing
- Remove backup logic

[2022-02]
- Deploy

[2022-01]
- Bootstrap template
- Entry add / List
- Dashboard / calculations / summary
- Graph





