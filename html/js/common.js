
// ## funcs
var restPromise = function(method, url, body){
	//console.log('DEBUG 1');
	return new Promise(function(resolve, reject){
		//console.log('DEBUG 2');
		
		var opts = {
			type: method,
			url: url,
			success: function(data){
				//console.log('restPromise - success - body = %j', data);
				return resolve(data);
			},
			error: function(err) {
				console.log(`${url} - err: ${JSON.stringify(err)}`);
				return reject(err);
			}
		};
		
		if( body ){
			opts.data = JSON.stringify(body);
			opts.contentType = "application/json; charset=utf-8";
		}
		
		$.ajax(opts);
	});
};
var getPromise = function(url){
	return restPromise('GET', url);
};
var postPromise = function(url, body){
	return restPromise('POST', url, body);
};
var putPromise = function(url, body){
	return restPromise('PUT', url, body);
};

var run_loop = function(func, time){
	func();
	return setInterval(func, time);
};

var tcell = function(val){
	return (`<td>${val}</td>`);
};
var trow = function(...vals){
	return '<tr>' + vals.map(x => tcell(x)).join('') + '</tr>';
};

var parseField = function(f, obj){
	var k = f.attr('id') || f.attr('name');
	var v = f.val();
	
	//console.log('DEBUG - k = %s | v = %s', k, v);
	if( v.length === 0 ){
		return false;
	}
	
	if( f.attr('type') === 'text' ){
		obj[ k ] = v;
	}
	if( f.attr('type') === 'number' ){
		obj[ k ] = Number(v);
	}
	
};

var collectForm = function(id){
	var fields = $(`#${id} :input`);
	var dropdowns = $(`#${id} select`);

	var collected = {};

	var eachDo = function(ind){
		var f = $(this);
		parseField(f, collected);
	};
	
	fields.each(eachDo);
	dropdowns.each(eachDo);
	
	return collected;
};

var getAccounts = async function(){
	var accounts = await getPromise('/api/accounts');
	return accounts.filter(x => x.level > 0);
};

var setAccountComplete = async function(accounts, opts){
	
	if( !opts ){
		opts = {};
	}
	
	if( !accounts ){
		accounts = await getPromise('/api/accounts');
		
		if( !opts.root ){
			accounts = accounts.filter(x => x.level > 0);
		}
	}
	
	console.log('setAccountComplete - start = %s', accounts.length);
	//var template_journal_row = await getPromise('/temp/journal-row.html');
	
	$('.complete-acc').autoComplete({
		minLength: 1,
		preventEnter: true,
		resolver: 'custom',
		events: {
			search: function(qry, callback){
				
				var queryLower = qry.toLowerCase();
				
				var matches = accounts.filter(function(x){
					return (
						x.name.toLowerCase().includes(queryLower)
						|| x.code.toLowerCase().includes(queryLower)
					);
				});
				
				var result = matches.map( x => x.name );
				return callback(result);
			}
		}
	});
	
	return accounts;
};


