
// ## dependencies
var db = require('../lib/db');

// ### export
var ns = {};
module.exports = ns;

// ## fund
ns.makeRowMonthly = function(obj){
	
	var row = '<tr>';
	
	row += '<td>' + obj.start + '</td>';
	row += '<td>' + obj.income + '</td>';
	row += '<td>' + obj.necessity + '</td>';
	row += '<td>' + obj.pc_n + ' % </td>';
	row += '<td>' + obj.save + '</td>';
	row += '<td>' + obj.pc_s + ' % </td>';
	row += '<td>' + obj.flexible + '</td>';
	row += '<td>' + obj.pc_f + ' % </td>';
	
	row += '<tr>';
	return row;
};
var shortDate = function(d){
	return d.toISOString().split('T')[0];
};
var round = function(num, p){
	var mult = Math.pow(10, p);
	return Math.round(num * mult)/mult;
};

ns.loadEntries = async function(req, res, next){
	console.log('routes/common.loadEntries - start...');
	var result = await db.entry.find({}, {limit: 50, sort: {'date': -1, 'added': -1} });
	res.locals.data = result;
	return next();
};
ns.calcMonthly = function(req, res, next){
	
	console.log('routes/common.calcMonthly - start...');
	var periodStart = 14;
	var monthLimit = 5;
	
	var dataList = res.locals.data.rows;
	var monthly = [];
	var date = new Date();
	date.setDate(periodStart);
	
	for(var i=0; i<monthLimit; i++){
		
		var obj = { start: shortDate(date) };
		
		date.setMonth( date.getMonth() + 1 );
		obj.end = shortDate(date);
		obj.income = 0;
		obj.necessity = 0;
		obj.save = 0;
		
		monthly.push(obj);
		date.setMonth( date.getMonth() - 2 );
	}
	
	dataList.forEach(function(x){
		monthly.forEach(function(m){
			if( x.date >= m.start && x.date < m.end ){
				//m.list.push( x );
				
				if( typeof(m[ x.category ]) !== 'undefined' ){
					m[ x.category ] += Number(x.amount);
				}
				
			}
		});
	});
	
	monthly.forEach(function(obj){
		obj.flexible = obj.income - obj.necessity - obj.save;
		
		obj.pc_n = 0;
		obj.pc_s = 0;
		obj.pc_f = 0;
		
		if( obj.income ){
			obj.pc_n = Math.round(obj.necessity / obj.income * 10000)/100;
			obj.pc_s = Math.round(obj.save / obj.income * 10000)/100;
			obj.pc_f = Math.round(obj.flexible / obj.income * 10000)/100;
		}
		
	});
	if( monthly[0].income === 0 && monthly[0].save === 0){
		monthly = monthly.slice(1);
	}
	
	res.locals.monthly = monthly;
	return next();
};


