
var init_entries = function(){
	var date = new Date();
	var shortDate = date.toISOString().split('T')[0];
	
	$('.form-entry').val('');
	$('#en-date').val(shortDate);
	$('.form-entry').blur();
	$('#msg').addClass('d-none');
	
};
var collectEntry = function(){
	var el_list = $('.form-entry');
	var obj = {};
	el_list.each(function(){
		var k = $(this).attr('id').replace('en-', '');
		var v = $(this).val();
		if( k === 'amount' ){
			v = v.replace(',', '.');
			v = Number(v);
		}
		obj[k] = v;
	});
	return obj;
};

var showMessage = function(msg, kind){
	$('#msg').text(msg);
	$('#msg').removeClass('d-none');
	setTimeout(function(){
		$('#msg').addClass('d-none');
	}, 8000);
};

var prepareGraphData = function(data){
	var inputData = data.sort(function(a,b){
		
		if( a.start > b.start ){
			return 1;
		}
		if( a.start < b.start ){
			return -1;
		}
		
		return 0;
		
	}).map(function(x){
		return {
			d: x.start.slice(0,7),
			n: x.pc_n,
			s: x.pc_s,
			f: x.pc_f
		};
	});
	console.log('inputData = %j', inputData);
	
	var lineColors = [
		{name: 'necessity', color: '#847DFA'},
		{name: 'save', color: '#F196B0'},
		{name: 'flexible', color: '#fd7e14'}
	];
	var ds = [];
	lineColors.forEach(function(c){
		ds.push({
			data: [],
			label: c.name,
			fill: false,
			borderColor: c.color,
			pointBackgroundColor: c.color,
			borderWidth: 3,
			pointStyle: 'circle',
			pointRadius: 4,
			pointBorderColor: 'transparent'
		});
	});
	
	var chartLabels = [];
	inputData.forEach(function(x){
		chartLabels.push( x.d.slice(0,7) );
		ds[0].data.push( x.n );
		ds[1].data.push( x.s );
		ds[2].data.push( x.f );
	});
	
	var chartData = {
		labels: chartLabels,
		type: 'line',
		defaultFontFamily: 'Montserrat',
		datasets: ds
	};
	return chartData;
};
var renderGraph = function(data){
	
	var chartData = prepareGraphData(data);
	
	var chartOptions = {
		responsive: !0,
		maintainAspectRatio: false,
		tooltips: {
			mode: 'index',
			titleFontSize: 12,
			titleFontColor: '#000',
			bodyFontColor: '#000',
			backgroundColor: '#fff',
			titleFontFamily: 'Montserrat',
			bodyFontFamily: 'Montserrat',
			cornerRadius: 3,
			intersect: false,
		},
		legend: {
			display: false,
			position: 'top',
			labels: {
				usePointStyle: true,
				fontFamily: 'Montserrat',
			},
		},
		scales: {
			xAxes: [{
				display: true,
				gridLines: {
					display: false,
					drawBorder: false
				},
				scaleLabel: {
					display: false,
					labelString: 'Month'
				}
			}],
			yAxes: [{
				display: true,
				gridLines: {
					display: false,
					drawBorder: false
				},
				scaleLabel: {
					display: false,
					labelString: 'Value'
				}
			}]
		},
		title: {
			display: false,
		}
	};
	var canvas = $("#chart_widget");
	
	var chart = new Chart(canvas, {
		type: 'line',
		data: chartData,
		options: chartOptions
	});
	
	return chart;
};

$(document).ready(function(){
	
	init_entries();
	
	$('.form-entry').on('keypress', async function(ev){
		if(ev.which == 13) {
			var field_id = $(this).attr('id');
			var len = $('#entry-tbody > tr').length;
			console.log('entry-form - enter - key = %s | len = %s', field_id, len );
			
			var obj = collectEntry();
			console.log('entry-form - collected = %j', obj );
			try{
				var result = await postPromise('/api/entry', obj);
				
				var addedRow = makeRowEntries(obj);
				$('#entry-tbody > tr').eq( len-1 ).before(addedRow);
				if( len > 11 ){
					$('#entry-tbody > tr').eq( len-1 ).remove();
				}
				
				init_entries();
				var up_month = await getPromise('/api/monthly?format=html');
				console.log('up_month = %j', up_month);
				$('#monthly-tbody').html(up_month.data);
				
				var up_month_json = await getPromise('/api/monthly?format=json');
				console.log('up_month_json = %j', up_month_json);
				
				var chartDataUp = prepareGraphData(up_month_json.data);
				chart.data = chartDataUp;
				chart.update();
				
			}
			catch(err){
				console.log('entry-form - catch - err.msg = %s', err.message);
				console.log('entry-form - catch - err = %j', err);
				showMessage(err.responseText);
			}
			
		}
	}); // end .key-press
	
	// ## distro
	var distro_fields = ['distro-income', 'distro-necessity', 'distro-save', 'distro-fun'];
	var distro_fileds_pc = distro_fields.slice(1);
	var distro_load = $('#distro-load').text();
	
	if( distro_load && distro_load.length ){
		console.log('distro_load = %j', distro_load);
		var distro_json = JSON.parse( distro_load );
		distro_fields.forEach(function(x){
			var k = x.replace('distro-', '');
			$('#' + x).val( distro_json[k] );
		});
		var distro_ts = new Date(distro_json.added);
		$('#distro-ts').text( distro_ts.toISOString().split('T')[0] );
	}
	
	$('.distro-tb').on("input", function(){
		var income_txt = $('#distro-income').val().trim().replace(',', '.');
		var income = Number( income_txt );
		console.log('income = ' + income);
		
		distro_fileds_pc.forEach(function(x){
			var pc = Number( $('#' + x).val() );
			var result = Math.round(income * pc) / 100;
			var distro_target_id = '#' + x + '-amount';
			console.log('result = %s | target = %s', result, distro_target_id);
			$(distro_target_id).text(result.toString().replace('.', ','));
		});
	});
	
	$('#distro-submit').click(async function(){
		
		var obj = {};
		distro_fields.forEach(function(x){
			var k = x.replace('distro-', '');
			var txt = $('#' + x).val().trim();
			var v = Number(txt);
			obj[k] = v;
		});
		
		console.log('distro - collected = %j', obj);
		try{
			var result = await postPromise('/api/distro', obj);
			console.log('distro - done - result = %j', result);
			var distro_ts_up = new Date(result.added);
			$('#distro-ts').text( distro_ts_up.toISOString().split('T')[0] );
		}
		catch(err){
			console.log('distro - catch - err.msg = %s', err.message);
			console.log('distro - catch - err = %j', err);
			showMessage(err.responseText);
		}
	});
	
	// Chart
	var monthly_txt = $('#monthly-load').text();
	console.log('monthly = %s', monthly_txt);
	var monthly_json = JSON.parse(monthly_txt);
	
	var chart = renderGraph(monthly_json);
	
});


